package com.example.quizv1cultureg;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Vector;

/**
 * Cette classe représente un jeu (un ensemble) de cartes de QCM.
 *
 * @author Virginie Galtier
 * @see Carte
 */
public class Quiz implements Serializable {
    
    private Vector<Carte> lesCartes;

    /**
     * Crée un nouveau quiz comportant 1 question
     */
    public Quiz() {
        this.lesCartes = new Vector<Carte>();

    }


    /**
     * Fournit un itérateur sur les questions du quiz
     *
     * @return itérateur sur les questions du quiz
     */
    public Iterator<Carte> getCartesIterator() {
        return lesCartes.iterator();
    }

    public void creerQuizDefaut(){
        Vector<String> propositionsIncorrectes = new Vector<String>();
        propositionsIncorrectes.add("Drachouny");
        propositionsIncorrectes.add("la Bête de la Seille");
        propositionsIncorrectes.add("la Tarasque");
        this.lesCartes.add(new Carte(
                "Comment s'appelle l'animal mythique à l'apparence d'un dragon qui vivait à Metz ?",
                "Le Graoully",
                propositionsIncorrectes));

        propositionsIncorrectes = new Vector<String>();
        propositionsIncorrectes.add("faux");
        this.lesCartes.add(new Carte(
                "Paul Verlaine est né à Metz.",
                "vrai",
                propositionsIncorrectes));

        propositionsIncorrectes = new Vector<String>();
        propositionsIncorrectes.add("la minette lorraine");
        propositionsIncorrectes.add("la pierre de Norroy");
        propositionsIncorrectes.add("la pierre d'Euville");
        this.lesCartes.add(new Carte(
                "Quelle pierre emblématique de la région est utilisée par de nombreux bâtiments de Metz ?",
                "la pierre de Jaumont",
                propositionsIncorrectes));

        propositionsIncorrectes = new Vector<String>();
        propositionsIncorrectes.add("rue de la tour aux rats");
        propositionsIncorrectes.add("rue du trou aux serpents");
        propositionsIncorrectes.add("rue du pas du loup");
        propositionsIncorrectes.add("rue aux ours");
        propositionsIncorrectes.add("rue du faisan");
        propositionsIncorrectes.add("rue de l'écrevisse");
        propositionsIncorrectes.add("rue de la chèvre");
        propositionsIncorrectes.add("rue des castors");
        this.lesCartes.add(new Carte(
                "Laquelle de ces rues n'existe pas à Metz ?",
                "rue des ânes",
                propositionsIncorrectes));

    }

    public void ajouter(Carte carte){
        this.lesCartes.add(carte);

    }
}
