package com.example.quizv1cultureg;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class FinActivity extends AppCompatActivity {
    private int score;
    private int nbrQuestions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin);
        Intent haveScore = getIntent();
        this.score = haveScore.getIntExtra("score",0);
        this.nbrQuestions = haveScore.getIntExtra("nbrQuestions",0);
        WebView maWebView = findViewById(R.id.office_tourisme);
        maWebView.setWebViewClient(new WebViewClient());
        maWebView.loadUrl("https://www.tourisme-metz.com/fr");

    }

    public void quitQuiz(View view) {
        finish();
    }

    public void sendMessage(View view) {
        Intent sendMsg = new Intent(Intent.ACTION_SENDTO);
        sendMsg.setData(Uri.parse("smsto:"));
        sendMsg.putExtra("message", "Dinguerie, j'ai eu un score de "+ score + " point(s) sur " + nbrQuestions + " question(s) au Quizz !");
        Intent chooseApp = Intent.createChooser(sendMsg, "Avec quelle app voulez vous envoyer votre score ?");
        startActivity(chooseApp);
    }
}