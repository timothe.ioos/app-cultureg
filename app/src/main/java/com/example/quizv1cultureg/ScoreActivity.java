package com.example.quizv1cultureg;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity implements View.OnClickListener {
    private int score;
    private int nbrQuestions;
    private SharedPreferences sp;
    private EditText pseudo;
    private LinearLayout ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        this.ll = findViewById(R.id.score_layout);
        Intent msg = getIntent();
        this.score = msg.getIntExtra("score",0);
        this.nbrQuestions = msg.getIntExtra("nbrQuestions",0);

        TextView endMessage = findViewById(R.id.endMessage);

        this.sp = getSharedPreferences("record",Context.MODE_PRIVATE);

        if(sp.contains("record") && sp.contains("pseudo")){
            int previousScore = sp.getInt("record",-1);
            String previousPseudo = sp.getString("pseudo","null");

            if(score>=previousScore){
                if(score > previousScore){
                    endMessage.setText(getString(R.string.best_establisher_message, previousPseudo, previousScore, nbrQuestions, score));
                    //endMessage.setText("Le record précédent établi par " + previousPseudo + " était " + previousScore + " point(s) / " + nbrQuestions + " question(s).\n" + "Vous battez ce record avec " + score + " points !\n" + "Entrez votre pseudo pour remplacer l'ancien record :");
                }else {
                    //endMessage.setText("Le record précédent établi par " + previousPseudo + " était " + previousScore + " point(s) / " + nbrQuestions + " question(s).\n" + "Vous égalez ce record \n" + "Entrez votre pseudo pour remplacer :");
                    endMessage.setText(getString(R.string.best_equalizer_message, previousPseudo, previousScore, nbrQuestions));

                }
                this.pseudo = new EditText(this);
                pseudo.setHint(R.string.enter_your_nickname);
                Button b = new Button(this);
                b.setText(R.string.ok);
                b.setId(R.id.register);
                b.setOnClickListener(this);
                ll.addView(pseudo);
                ll.addView(b);



            } else {
                // endMessage.setText( "Le record est actuellement détenu par " + previousPseudo + " avec " + previousScore + " point(s) / " + nbrQuestions + "question(s) .\n Vous êtes en  avec " + score + " point(s)...");
                endMessage.setText(getString(R.string.best_lower_message, previousPseudo, previousScore, nbrQuestions, score));

                Button b2 = new Button(this);
                b2.setText(R.string.ok);
                b2.setId(R.id.no_register);

                b2.setOnClickListener(this);
                ll.addView(b2);
            }

        } else {
            endMessage.setText(getString(R.string.first_best_message, score, nbrQuestions));

            // endMessage.setText("Vous êtes le premier joueur à terminer le quiz. Vous établissez le premier record avec " + score + " point(s) sur " + nbrQuestions + " question(s)! Entrez votre pseudo pour enregistrer ce record :");
            this.pseudo = new EditText(this);
            pseudo.setHint(R.string.enter_your_nickname);
            Button b = new Button(this);
            b.setText(R.string.ok);
            b.setId(R.id.register);
            b.setOnClickListener(this);
            ll.addView(pseudo);
            ll.addView(b);

        }


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.register){
            SharedPreferences.Editor editor = sp.edit();
            editor.putInt("record", score);
            editor.putString("pseudo", pseudo.getText().toString());
            editor.apply();
        }
        Intent sendQuit = new Intent();
        sendQuit.setClass(this, FinActivity.class);
        sendQuit.putExtra("score", score);
        sendQuit.putExtra("nbrQuestions", nbrQuestions);
        startActivity(sendQuit);
        finish();
        //sendQuit.putExtra();

    }
}