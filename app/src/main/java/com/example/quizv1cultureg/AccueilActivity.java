package com.example.quizv1cultureg;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import cjh.WaveProgressBarlibrary.WaveProgressBar;

public class AccueilActivity extends AppCompatActivity {
    public static Quiz quiz;

    int progress = 0;
    boolean started = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        this.quiz = new Quiz();


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        Response.Listener<String> responseListener = new TrivialResponseListener();
        Response.ErrorListener errorListener = new TrivialErrorListener();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "https://the-trivia-api.com/v2/questions/", responseListener, errorListener);
        requestQueue.add(stringRequest);
    }

    private class TrivialResponseListener implements Response.Listener<String> {
        @Override
        public void onResponse(String response) {
            try{
                Log.i("json", response);

                JSONArray jsonArray = new JSONArray(response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String correctAnswer = jsonObject.getString("correctAnswer");

                    JSONArray incorrectAnswersArray = jsonObject.getJSONArray("incorrectAnswers");
                    Vector<String> incorrectAnswersVector = new Vector<>();

                    // Convertir le tableau incorrectAnswers en Vector
                    for (int j = 0; j < incorrectAnswersArray.length(); j++) {
                        incorrectAnswersVector.add(incorrectAnswersArray.getString(j));
                    }
                    JSONObject questionObject = jsonObject.getJSONObject("question");
                    String questionText = questionObject.getString("text");
                    quiz.ajouter(new Carte(questionText,correctAnswer, incorrectAnswersVector));

                }
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), CarteActivity.class);
                intent.putExtra("quiz",quiz);
                startActivity(intent);
                finish();

            } catch (JSONException e) {
                Log.i("C'est la sauce", "sauce");

                quiz.creerQuizDefaut();

            }

        }
    }

    private class TrivialErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.i("Erreur", "Requete échouée");
            quiz.creerQuizDefaut();

            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), CarteActivity.class);
            intent.putExtra("quiz",quiz);
            startActivity(intent);
            finish();
        }
    }
}