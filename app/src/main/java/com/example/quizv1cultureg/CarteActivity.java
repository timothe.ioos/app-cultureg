package com.example.quizv1cultureg;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

public class CarteActivity extends AppCompatActivity implements View.OnClickListener {
    private Carte carte;
    private Quiz quiz;
    private Iterator iteratorCard;
    private LinearLayout ll;
    private Vector<String> listQuestions;
    private MediaPlayer mp;
    private int score;
    private int nbrQuestions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carte);
        mp = MediaPlayer.create(this, R.raw.entertainer);
        mp.start();
        Intent intent = getIntent();
        this.quiz = (Quiz) intent.getSerializableExtra("quiz");
        this.ll = findViewById(R.id.monLayout);

        this.iteratorCard = quiz.getCartesIterator();
        if(iteratorCard.hasNext()){
            this.carte = (Carte) iteratorCard.next();
            afficherCarte();

        } else{
            Log.i("ErreurCarte", "Erreur pas de cartes à afficher");
        }

    }
    private void afficherCarte() {
        // récupération de la prochaine carte
        // affichage de la question de la carte
        // création d'une liste *mélangée* des propositions de réponses (correcte et incorrectes)
        // affichage d'une proposition par bouton
        nbrQuestions++;
        ll.removeAllViews();
        TextView tvMessage = new TextView(this);
        tvMessage.setText(carte.getQuestion());
        tvMessage.setTextSize(20);
        ll.addView(tvMessage);


        this.listQuestions = new Vector<>(carte.getPropositionsIncorrectes());
        this.listQuestions.add(carte.getReponseCorrecte());
        Collections.shuffle(this.listQuestions);
        Iterator iteratorQuestions = this.listQuestions.iterator();
        int i = 0;
        while (iteratorQuestions.hasNext()){
            Button b = new Button(this);
            b.setOnClickListener(this);
            b.setText(iteratorQuestions.next().toString());
            b.setTextSize(20);
            ll.addView(b);
            b.setId(i);
            i++;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mp.isPlaying()){
            mp.pause();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        mp.start();

    }

    @Override
    public void onClick(View view) {
        Button bouton = (Button) view;
        String message = bouton.getText().toString();
        if(message.equals(carte.getReponseCorrecte())){
            score++;
            message = "Bonne réponse";}
        else  {
            message = "Mauvaise réponse";

        }
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

        if(iteratorCard.hasNext()){
            this.carte = (Carte) iteratorCard.next();
            afficherCarte();

        } else {
            ll.removeAllViews();
            if (mp.isPlaying()){
                mp.stop();
            }
            Intent endMsg = new Intent();
            endMsg.setClass(this, ScoreActivity.class);
            endMsg.putExtra("score", score);
            endMsg.putExtra("nbrQuestions", nbrQuestions);
            startActivity(endMsg);
            finish();

        }
    }
}